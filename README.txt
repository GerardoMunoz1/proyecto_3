# Proyecto III - Unidad II

Profesor: Fabio Durán.
Estudiante: Gerardo Muñoz.
Carrera: Ingeniería Civil en Bioninformática.

- Visualizador de moléculas 1.0, es un programa con la finalidad de mostrar
gráficamente las moléculas con formato mol o mol2 que el usuario estime conveniente.
Esta es la solución propuesta (no completa), al problema planteado donde se
solicitaba construir un visualizador de moléculas con formato mol y/o mol2.

# Procesos positivos dentro del uso
- Al iniciar el programa, se solicita un archivo (carpeta o formato mol, mol2).

- Si el programa, detecta que no se ha seleccionado ninguna ruta, esta no permitirá
el acceso a "Visualizar molécula", mientras que en la terminal, imprimirá un
"[ERROR]", indicando así que falta una ruta por seleccionar.

- Al ingresar una ruta, dicha ruta se guarda y sale disponible como información
dentro de la ventana de "Visualizar molécula", dando como información al usuario
en que directorio se encuentra la molécula especificada.

# Problemas y procesos no completados
- El programa en sí, no cumple lo pedido inicialmente, ya que no muestra la imagen
ni la información de la molécula escogida.

# Referencias de la PEP-8
- Solo se usan espacios, no se usan tabulaciones.
- Los comentarios, están escritos sobre cada clase, dando así una mayor claridad
del código.
