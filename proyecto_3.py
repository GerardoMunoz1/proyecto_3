import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import json
import os
import sys
from os import remove
os.system('pymol Flavonoids_2_Genistin.mol2 -x -g ejemplo2.png -c -Q')

# Método para abrir archivos json
def open_file():
	try:
		with open('rute.json', 'r') as file:
			data = json.load(file)
		file.close()
	except IOError:
		data = []

	return data
# Método para guardar archivos json
def save_file(data):
    print("Save File")

    # Guardamos todos los datos enviados en 'data' en formato Json
    with open('rute.json', 'w') as file:
        json.dump(data, file, indent=4)
    file.close()
# Main_core
class main():

	def __init__(self):
		print("Contructor!")
		self.builder = Gtk.Builder()

		self.builder.add_from_file("proyecto_3.glade")
		self.main = self.builder.get_object("main")
		self.main.connect("destroy", Gtk.main_quit)
		self.main.set_default_size(800, 400)
		self.main.set_title("Visualizador de moleculas 1.0")

		self.Archivo = self.builder.get_object("abrirDirectorio")
		self.Archivo.connect("clicked", self.ventanaDirectorio)

		self.Cerrar = self.builder.get_object("botonCerrar")
		self.Cerrar.connect("clicked", self.cerrar)

		self.AcercaDe = self.builder.get_object("boton_about")
		self.AcercaDe.connect("clicked", self.About)

		self.verMol = self.builder.get_object("visualizar_Molecula")
		self.verMol.connect("clicked", self.Visualizar)

		self.main.show_all()

	def ventanaDirectorio(self, btn=None):
		print("Abriendo directorio para escoger archivo...")
		aux = Directorio()

	def About(self, btn=None):
		print("\nAcerca de...")
		aux = info()

	def Visualizar(self, btn=None):
		f = open_file()

		if (f == []):
			print("\n[ERROR] SELECCIONE UN DIRECTORIO.")

		else:
			print("\nVisualizando molécula...")
			aux = visualizadorinfo()

	def cerrar(self, btn=None):
		print("\nCerrando...\n")
		self.main.destroy()
		remove("rute.json")
# Selector de archivos
class Directorio():

	def __init__(self):
		print("\n\nBIENVENIDO AL DIRECTORIO...")
		self.builder = Gtk.Builder()
		self.builder.add_from_file("proyecto_3.glade")
		self.ruta = self.builder.get_object("fileChoose")
		self.ruta.set_default_size(400, 400)
		self.ruta.set_title("Directorio")

		self.CancelarDir = self.builder.get_object("botonCancel")
		self.CancelarDir.connect("clicked", self.Cancelar)

		self.AñadirDir = self.builder.get_object("botonAñadir")
		self.AñadirDir.connect("clicked", self.Añadir)
	
		self.LabelRuta = self.builder.get_object("label_ModificarDirectorio")

		filtro = Gtk.FileFilter()
		filtro.set_name("mol Files")
		filtro.add_pattern("*.pdb")
		filtro.add_pattern("*.mol2")
		filtro.add_pattern("*.mol")

		self.ruta.add_filter(filtro)

		filtro = Gtk.FileFilter()
		filtro.set_name("All Files")
		filtro.add_pattern("*")

		self.ruta.add_filter(filtro)

		self.ruta.show_all()

	def Añadir(self, btn=None):
		print("\nAñadiendo archivo...")

		self.directorio_final = self.ruta.get_filename()
		print("\nLa ruta actual es:", self.directorio_final)

		j = {"": self.directorio_final}

		f = open_file()
		f.append(j)
		save_file(f)

		self.ruta.destroy()

	def Cancelar(self, btn=None):
		print("\nCancelando archivo...")
		self.ruta.destroy()

# Visualizador final de la molécula
class visualizadorinfo():

	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("proyecto_3.glade")
		self.ver = self.builder.get_object("info_molecula")
		self.ver.set_default_size(400, 400)
		self.ver.set_title("Visualizador")

		self.cancelarmol = self.builder.get_object("cancelar_info")
		self.cancelarmol.connect("clicked", self.cerrarmol)

		self.guardarmol = self.builder.get_object("guardar_info")
		self.guardarmol.connect("clicked", self.guardaMOL)
		
		self.directorio_info = self.builder.get_object("label_RutaFinal")

		f = open_file()
		self.directorio_info.set_text(str(f))

		self.ver.show_all()

	def guardaMOL(self, btn=None):
		print("\nGuardando molécula...")

	def cerrarmol(self, btn=None):
		print("\nCancelando información de molécula...")
		self.ver.destroy()
		remove("rute.json")
# About
class info():

	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("proyecto_3.glade")
		self.about = self.builder.get_object("dialogoAbout")
		self.about.set_default_size(400, 400)
		self.about.set_title("About")

		self.Close = self.builder.get_object("botoncerrarAbout")
		self.Close.connect("clicked", self.CerrarAbout)

		self.about.show_all()

	def CerrarAbout(self, btn=None):
		print("\nCerrando Acerca de...")
		self.about.destroy()

# Main
if __name__ == '__main__':
	F = main()
	Gtk.main()